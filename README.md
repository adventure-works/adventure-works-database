# Projet rattrapage - AdventureWorks
## A propos du projet

Cette stack permet au projet AdventureWorks de fournir la base de données pour l'architecture microservices.
Automatiquement la base de données sera importée par défaut à l'installation du conteneur.

### Getting Started
Voici les différentes étapes listées ci-dessous pour pouvoir obtenir une copie locale opérationnelle.

### Prérequis
* **[Docker-Compose](https://docs.docker.com/compose/install/)**

### Installation

#### Installation et lancement du conteneur via un *terminal*.

+ Clonage du projet
````bash
git clone https://gitlab.com/adventure-works/adventure-works-database.git
````

+ Se déplacer dans le projet
````bash
cd database
````

+ Lancement du conteneur

````bash
docker-compose up -d
````

:warning: ***Si toutefois l'importation ne s'est pas effectuée correctement lors de l'installation, veuillez vous référez à la partie <a href="#utilisation">Utilisation</a>.*** :warning:

### :globe_with_meridians: **Visualisation de la base de données sur navigateur via [PhpMyAdmin](http://localhost:82/).** :globe_with_meridians:

### Utilisation

+ Interagir avec le conteneur via un **terminal**.

````bash
#### Connexion à l'image MySql
docker exec -it adventureworks-database mysql -u adventureuser -p 
````

> **Entrer le mot de passe** : *password*

````bash
#### Utilisation de la base en question
mysql> use adventureworks;

#### Importation manuelle de la base de données
mysql> source /docker-entrypoint-initdb.d/AdventureWorks.sql;

#### Quitter l'image MySql
mysql > exit;
````

